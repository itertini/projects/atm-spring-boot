# ATM API Spring Boot

The ATM API is a robust and scalable backend service designed using the Spring Boot framework. It provides essential functionalities required for managing ATM operations, ensuring seamless interactions between users and their bank accounts. Key features of the ATM API include:

    Account Management:
        Account Balance Inquiry: Allows users to check the current balance of their bank accounts.
        Account Statement: Provides detailed transaction history for user accounts.

    Transaction Handling:
        Cash Withdrawal: Supports secure withdrawal of funds from user accounts with necessary validations and limits.
        Cash Deposit: Facilitates the deposit of cash into user accounts, updating the balance in real-time.
        Funds Transfer: Enables transfer of funds between accounts, ensuring transactional integrity and security.

Coming soon:    
    Spring security:
        Implementing Spring security to ensure that the Deposit, Transfer, Withdraw endpoints work the correctly.


## Created with:
    - Java
    - Spring Boot
    - Hibernate
    - Lombock
    - MariaDB


# Project Setup

## Prerequisites

    - Java 11 or higher
    - Maven

## Installation

Clone the repository:
```
git clone https://gitlab.com/itertini/projects/atm-spring-boot/
cd atm-spring-boot
```

Build the project:
```
mvn clean install
```


Start the application:
```
mvn spring-boot:run
```

## Endpoints

### Create a New Customer

**Endpoint:** `POST /api/customer`

**Description:** Creates a new user with the specified username.

**RequestBody:**
```json
{
    "firstName": "John",
    "lastName": "Doe",
    "email": "johndoe@example.com"
}
```

**Response:**
```json
{
    "customerId": 1,
    "firstName": "John",
    "lastName": "Doe",
    "email": "johndoe@example.com"
}
```


### Get All Customers

**Endpoint:** `GET /api/customer`

**Description:** Retrieves a list of all customers.

**Response:**
- `username`: The username of the customer.
- `customerId`: The unique ID of the customer.

**Example:**
```json
[
    {
        "customerId": 1,
        "firstName": "John",
        "lastName": "Doe",
        "email": "johndoe@example.com"
    },
    {
        "customerId": 2,
        "firstName": "Jane",
        "lastName": "Doe",
        "email": "janedoe@example.com"
    }
]
```

### Get Customer Details

**Endpoint:**  `POST /api/customer/{customerId}`

**Description:** Get details of a single customer

**RequestBody:**
none

**Response:**
```json
{
    "customerId": 1,
    "firstName": "John",
    "lastName": "Doe",
    "email": "johndoe@example.com",
    "accounts": [
        {
            "accountId": 1,
            "balance": 900
        },
        {
            "accountId": 2,
            "balance": 2100
        }
    ]
}
```

### Create a New Account

**Endpoint:**  `POST /api/account`

**Description:** Creates an Account for a customer.

**RequestBody:**
```json
{
    "customerId": 1
}
```

**Response:**
```json
{
    "accountId": 3,
    "balance": 0,
    "owner": {
        "customerId": 1,
        "firstName": "John",
        "lastName": "Doe",
        "email": "johndoe@example.com"
    }
}
```

### Get Account Details

**Endpoint:**  `POST /api/account/{accountId}`

**Description:** Gets the account details .

**RequestBody:**
none

**Response:**
```json
{
    "accountId": 1,
    "balance": 900,
    "owner": {
        "customerId": 1,
        "firstName": "John",
        "lastName": "Doe",
        "email": "johndoe@example.com"
    }
}
```

### Deposit

**Endpoint:**  `POST /api/account/{accountId}/deposit`

**Description:** Deposit an amount on an account

**RequestBody:**
```json
{
    "amount": 1000 
}
```

**Response:**
```json
{
    "transactionId": 1,
    "senderId": 1,
    "receiverId": 1,
    "amount": 1000,
    "date": "17-06-2024",
    "type": "Deposit"
}
```

### Transfer

**Endpoint:**  `POST /api/account/{accountId}/transfer`

**Description:** Transfers an amount to someone else account.
**RequestBody:**
```json
{
    "receiverId": 2,
    "amount": 100
}
```

**Response:**
```json
{
    "transactionId": 2,
    "senderId": 1,
    "receiverId": 2,
    "amount": 100,
    "date": "17-06-2024",
    "type": "Transfer"
}
```

### Withdraw

**Endpoint:**  `POST /api/account/{accountId}/withdraw`

**Description:** Customer can withdraw an amount from his Account

**RequestBody:**
```json
{
    "amount": 200
}
```

**Response:**
```json
{
    "transactionId": 3,
    "senderId": 1,
    "receiverId": 1,
    "amount": 200,
    "date": "17-06-2024",
    "type": "Withdraw"
}
```

### Get Transactions

**Endpoint:**  `POST /api/account/{accountId}/transactions`

**Description:** Get all transactions from an account

**RequestBody:**
none

**Response:**
```json
[
        {
        "transactionId": 1,
        "senderId": 1,
        "receiverId": 1,
        "amount": 1000,
        "date": "17-06-2024",
        "type": "Deposit"
    },
    {
        "transactionId": 2,
        "senderId": 1,
        "receiverId": 2,
        "amount": 100,
        "date": "17-06-2024",
        "type": "Transfer"
    },
    {
        "transactionId": 3,
        "senderId": 1,
        "receiverId": 1,
        "amount": 200,
        "date": "17-06-2024",
        "type": "Withdraw"
    }
]
```