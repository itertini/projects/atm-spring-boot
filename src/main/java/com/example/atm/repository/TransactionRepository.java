package com.example.atm.repository;

import com.example.atm.entity.Account;
import com.example.atm.entity.Transaction;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TransactionRepository extends JpaRepository<Transaction, Long> {

    @Query("SELECT t FROM Transaction t WHERE t.sender= :sender or t.receiver= :receiver")
    List<Transaction> getAllBySenderOrReceiver(Account sender, Account receiver);

    List<Transaction> findBySenderAndReceiverAndAmountAndType(Account sender, Account receiver, long amount, String type);
}
