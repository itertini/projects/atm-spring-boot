package com.example.atm.service;

import com.example.atm.dto.transaction.TransactionDTO;
import com.example.atm.entity.Account;
import com.example.atm.entity.Transaction;
import com.example.atm.enums.TransactionType;
import com.example.atm.repository.AccountRepository;
import com.example.atm.repository.TransactionRepository;
import com.example.atm.util.ConversionUtil;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service
@RequiredArgsConstructor
public class TransactionService {

    private final TransactionRepository transactionRepository;

    private final ConversionUtil conversionUtil;

    public TransactionDTO saveTransaction(Account sender, Account receiver, long amount, TransactionType type){
        Transaction transaction = buildTransaction(sender, receiver, amount);
        transaction.setType(type);
        transactionRepository.save(transaction);
        return buildDTO(transaction);
    }

    public Transaction buildTransaction(Account sender, Account receiver, long amount) {
        return Transaction.builder()
                .sender(sender)
                .receiver(receiver)
                .amount(amount)
                .date(conversionUtil.formatDate(new Date()))
                .build();
    }

    public TransactionDTO buildDTO(Transaction transaction) {
        return TransactionDTO.builder()
                .transactionId(transaction.getTransactionId())
                .senderId(transaction.getSender().getAccountId())
                .receiverId(transaction.getReceiver().getAccountId())
                .amount(transaction.getAmount())
                .date(transaction.getDate())
                .type(transaction.getType().getLabel())
                .build();
    }

    public List<Transaction> getAllBySenderOrReceiver(Account sender, Account receiver) {
        return transactionRepository.getAllBySenderOrReceiver(sender, receiver);
    }

}

