package com.example.atm.service;

import com.example.atm.dto.account.CreateAccountDTO;
import com.example.atm.dto.transaction.DepositTransactionDTO;
import com.example.atm.dto.account.DetailAccountDTO;
import com.example.atm.dto.customer.ResponseCustomerDTO;
import com.example.atm.dto.transaction.TransactionDTO;
import com.example.atm.dto.transaction.TransferTransactionDTO;
import com.example.atm.dto.transaction.WithdrawTransactionDTO;
import com.example.atm.entity.Account;
import com.example.atm.entity.Customer;
import com.example.atm.enums.TransactionType;
import com.example.atm.repository.AccountRepository;
import com.example.atm.repository.CustomerRepository;
import com.example.atm.util.ConversionUtil;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class AccountService {

    private final AccountRepository accountRepository;

    private final CustomerRepository customerRepository;

    private final TransactionService transactionService;

    private final ConversionUtil conversionUtil;

    // POST /api/account
    public DetailAccountDTO createAccount(CreateAccountDTO createAccountDTO) {
        Customer customer = conversionUtil.getFromOptional(customerRepository.findById(createAccountDTO.getCustomerId()));
        Account account = accountRepository.save(Account.builder()
                        .balance(0)
                        .customer(customer)
                        .build());
        return buildAccountDTO(account, customer);
    }

    // GET /api/account/{accountId}
    public DetailAccountDTO getAccountById(long accountId) {
        Account account = conversionUtil.getFromOptional(accountRepository.findById(accountId));
        return buildAccountDTO(account, account.getCustomer());

    }

    // POST /api/account/{accountId}/deposit
    public TransactionDTO deposit(long accountId, DepositTransactionDTO depositTransactionDTO) {
        Account account = conversionUtil.getFromOptional(accountRepository.findById(accountId));
        account.setBalance(account.getBalance() + depositTransactionDTO.getAmount());
        accountRepository.save(account);
        return transactionService.saveTransaction(account, account, depositTransactionDTO.getAmount(), TransactionType.DEPOSIT);
    }

    // POST /api/account/{accountId}/transfer
    public TransactionDTO transfer(long accountId, TransferTransactionDTO transferTransactionDTO) {
        Account sender = findAccountById(accountId);
        Account receiver = findAccountById(transferTransactionDTO.getReceiverId());
        long amount = transferTransactionDTO.getAmount();

        if (sender.getBalance() < transferTransactionDTO.getAmount()){
            throw new RuntimeException();
        }
        sender.setBalance(sender.getBalance() - amount);
        receiver.setBalance(receiver.getBalance() + amount);

        return transactionService.saveTransaction(sender, receiver, amount, TransactionType.TRANSFER);
    }

    // POST /api/account/{accountId}/withdraw
    public TransactionDTO withdraw(long accountId, WithdrawTransactionDTO withdrawTransactionDTO) {
        Account account = findAccountById(accountId);
        long amount = withdrawTransactionDTO.getAmount();

        if (account.getBalance() < amount) {
            throw new RuntimeException();
        }
        account.setBalance(account.getBalance() - amount);
        accountRepository.save(account);
        return transactionService.saveTransaction(account, account, amount, TransactionType.WITHDRAW);
    }

    // GET /api/account/{accountId}/transactions
    public List<TransactionDTO> getAccountTransactions(long accountId) {
        Account account = findAccountById(accountId);
        List<TransactionDTO> transactions = new ArrayList<>();
        transactionService.getAllBySenderOrReceiver(account, account).forEach(transaction -> {
            transactions.add(transactionService.buildDTO(transaction));
        });
        return transactions;
    }

    public Account findAccountById(long accountId) {
        return conversionUtil.getFromOptional(accountRepository.findById(accountId));
    }

    public DetailAccountDTO buildAccountDTO(Account account, Customer customer) {
        ResponseCustomerDTO responseCustomerDTO = ResponseCustomerDTO.builder()
                .customerId(customer.getCustomerId())
                .firstName(customer.getFirstName())
                .lastName(customer.getLastName())
                .email(customer.getEmail())
                .build();
        return DetailAccountDTO.builder()
                .accountId(account.getAccountId())
                .balance(account.getBalance())
                .owner(responseCustomerDTO)
                .build();
    }
}
