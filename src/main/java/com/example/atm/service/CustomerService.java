package com.example.atm.service;

import com.example.atm.dto.account.ResponseAccountDTO;
import com.example.atm.dto.customer.CreateCustomerDTO;
import com.example.atm.dto.customer.DetailCustomerDTO;
import com.example.atm.dto.customer.ResponseCustomerDTO;
import com.example.atm.entity.Customer;
import com.example.atm.repository.AccountRepository;
import com.example.atm.repository.CustomerRepository;
import com.example.atm.util.ConversionUtil;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.ArrayList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class CustomerService {

    private final CustomerRepository customerRepository;

    private final AccountRepository accountRepository;

    private final ConversionUtil conversionUtil;


    // POST /api/customer
    public ResponseCustomerDTO createCustomer(CreateCustomerDTO createCustomerDTO) {

        Customer customer = Customer.builder()
                .firstName(createCustomerDTO.getFirstName())
                .lastName(createCustomerDTO.getLastName())
                .email(createCustomerDTO.getEmail())
                .build();
        customerRepository.save(customer);

        return ResponseCustomerDTO.builder()
                .customerId(customer.getCustomerId())
                .firstName(customer.getFirstName())
                .lastName(customer.getLastName())
                .email(customer.getEmail())
                .build();
    }

    // GET /api/customer
    public List<ResponseCustomerDTO> getAllCustomers() {
        List<ResponseCustomerDTO> customerList = new ArrayList<>();
        customerRepository.findAll().forEach(customer -> {
            customerList.add(ResponseCustomerDTO.builder()
                            .customerId(customer.getCustomerId())
                            .firstName(customer.getFirstName())
                            .lastName(customer.getLastName())
                            .email(customer.getEmail())
                            .build());
        });
        return customerList;
    }

    // GET /api/customer/{customerId}
    public DetailCustomerDTO getCustomerDetails(long customerId) {
        Customer customer = conversionUtil.getFromOptional(customerRepository.findById(customerId));
        List<ResponseAccountDTO> accounts = new ArrayList<>();

        customer.getAccount().forEach(account -> {
            accounts.add(ResponseAccountDTO.builder()
                            .accountId(account.getAccountId())
                            .balance(account.getBalance())
                            .build());
        });
        return DetailCustomerDTO.builder()
                .customerId(customerId)
                .firstName(customer.getFirstName())
                .lastName(customer.getLastName())
                .email(customer.getEmail())
                .accounts(accounts)
                .build();
    }

}
