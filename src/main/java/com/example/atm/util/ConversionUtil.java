package com.example.atm.util;

import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.NoSuchElementException;
import java.util.Optional;

@Service
public class ConversionUtil {

    public <T> T getFromOptional(Optional<T> optional) {
        if (optional.isEmpty()) {
            throw new NoSuchElementException();
        }
        return optional.get();
    }

    public String formatDate(Date date) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");
        return simpleDateFormat.format(date);
    }
}
