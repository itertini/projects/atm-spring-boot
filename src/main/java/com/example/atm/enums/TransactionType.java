package com.example.atm.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum TransactionType {

    TRANSFER("Transfer"),
    DEPOSIT("Deposit"),
    WITHDRAW("Withdraw");

    private final String label;

}
