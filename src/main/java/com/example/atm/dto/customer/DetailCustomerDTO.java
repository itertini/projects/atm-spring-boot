package com.example.atm.dto.customer;

import com.example.atm.dto.account.ResponseAccountDTO;
import lombok.*;

import java.util.List;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class DetailCustomerDTO {

    private long customerId;

    private String firstName;

    private String lastName;

    private String email;

    private List<ResponseAccountDTO> accounts;
}
