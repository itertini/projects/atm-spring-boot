package com.example.atm.dto.customer;

import lombok.*;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class ResponseCustomerDTO {

    private long customerId;

    private String firstName;

    private String lastName;

    private String email;
}
