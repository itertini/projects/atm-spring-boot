package com.example.atm.dto.transaction;

import lombok.*;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class TransferTransactionDTO {

    private long receiverId;

    private long amount;

}
