package com.example.atm.dto.transaction;

import lombok.*;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class TransactionDTO {

    private long transactionId;

    private long senderId;

    private long receiverId;

    private long amount;

    private String date;

    private String type;
}
