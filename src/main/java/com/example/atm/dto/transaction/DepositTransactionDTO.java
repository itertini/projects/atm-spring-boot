package com.example.atm.dto.transaction;

import lombok.*;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class DepositTransactionDTO {

    private long amount;

}
