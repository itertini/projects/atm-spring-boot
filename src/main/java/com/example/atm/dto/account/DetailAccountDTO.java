package com.example.atm.dto.account;

import com.example.atm.dto.customer.ResponseCustomerDTO;
import lombok.*;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class DetailAccountDTO {

    private long accountId;

    private long balance;

    private ResponseCustomerDTO owner;

}
