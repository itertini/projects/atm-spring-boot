package com.example.atm.dto.account;

import lombok.*;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class CreateAccountDTO {

    private long customerId;

}
