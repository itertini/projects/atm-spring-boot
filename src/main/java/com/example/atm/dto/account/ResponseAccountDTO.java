package com.example.atm.dto.account;


import lombok.*;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class ResponseAccountDTO {

    private long accountId;

    private long balance;
}
