package com.example.atm.controller;

import com.example.atm.dto.account.CreateAccountDTO;
import com.example.atm.dto.transaction.DepositTransactionDTO;
import com.example.atm.dto.transaction.TransferTransactionDTO;
import com.example.atm.dto.transaction.WithdrawTransactionDTO;
import com.example.atm.service.AccountService;
import lombok.RequiredArgsConstructor;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RequiredArgsConstructor
@RestController
@RequestMapping("/api/account")
public class AccountController {

    private final AccountService accountService;

    @PostMapping
    public ResponseEntity<?> createAccount(@RequestBody CreateAccountDTO createAccountDTO) {
        try {
            return new ResponseEntity<>(accountService.createAccount(createAccountDTO), HttpStatus.OK);
        } catch (DataIntegrityViolationException e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.CONFLICT);
        }
    }

    @GetMapping("/{accountId}")
    public ResponseEntity<?> getAccountById(@PathVariable long accountId) {
        try {
            return new ResponseEntity<>(accountService.getAccountById(accountId), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping("/{accountId}/deposit")
    public ResponseEntity<?> deposit(@PathVariable long accountId, @RequestBody DepositTransactionDTO depositTransactionDTO) {
        try {
            return new ResponseEntity<>(accountService.deposit(accountId, depositTransactionDTO), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping("/{accountId}/transfer")
    public ResponseEntity<?> transfer(@PathVariable long accountId, @RequestBody TransferTransactionDTO transferTransactionDTO) {
        try {
            return new ResponseEntity<>(accountService.transfer(accountId, transferTransactionDTO), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping("/{accountId}/withdraw")
    public ResponseEntity<?> withdraw(@PathVariable long accountId, @RequestBody WithdrawTransactionDTO withdrawTransactionDTO) {
        try {
            return new ResponseEntity<>(accountService.withdraw(accountId, withdrawTransactionDTO), HttpStatus.OK);
        } catch (RuntimeException e) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/{accountId}/transactions")
    public ResponseEntity<?> getAccountTransactions(@PathVariable long accountId) {
        try {
            return new ResponseEntity<>(accountService.getAccountTransactions(accountId), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

}
