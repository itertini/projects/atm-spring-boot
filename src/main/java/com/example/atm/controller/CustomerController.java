package com.example.atm.controller;

import com.example.atm.dto.customer.CreateCustomerDTO;
import com.example.atm.dto.customer.ResponseCustomerDTO;
import com.example.atm.service.CustomerService;
import lombok.RequiredArgsConstructor;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/customer")
@RequiredArgsConstructor
public class CustomerController {

    private final CustomerService customerService;

    @PostMapping
    public ResponseEntity<?> createCustomer(@RequestBody CreateCustomerDTO createCustomerDTO) {
        try {
            return new ResponseEntity<>(customerService.createCustomer(createCustomerDTO), HttpStatus.OK);
        } catch (DataIntegrityViolationException e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping
    public ResponseEntity<List<ResponseCustomerDTO>> getAllCustomers() {
            return new ResponseEntity<>(customerService.getAllCustomers(), HttpStatus.OK);
    }

    @GetMapping("{customerId}")
    public ResponseEntity<?> getCustomerDetails(@PathVariable long customerId) {
        try {
            return new ResponseEntity<>(customerService.getCustomerDetails(customerId), HttpStatus.OK);
        } catch (DataIntegrityViolationException e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }
}
